package com.mycompany.simpleapp;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class MainActivityFragment extends Fragment {

    private static final String TAG = "MainActivityFragment";
    private Button startButton, stopButton;
    private TextView resultsTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main,
                container, false);
        startButton = (Button) rootView.findViewById(R.id.startButton);
        stopButton = (Button) rootView.findViewById(R.id.stopButton);
        resultsTextView = (TextView) rootView
                .findViewById(R.id.resultsTextView);
        View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v == startButton) {
                    Log.v(TAG, "Start Service Button clicked");
                    ((MainActivity) getActivity())
                            .startGenerating();
                } else {
                    Log.v(TAG, "Stop Service Button clicked");
                    ((MainActivity) getActivity())
                            .stopGenerating();
                }
            }
        };
        startButton.setOnClickListener(listener);
        stopButton.setOnClickListener(listener);
        return rootView;
    }

    public TextView getResultsTextView() {
        return resultsTextView;
    }

    public void updateText(String results){
        ((MainActivity)getActivity()).updateResults(results);
    }
}
