package com.mycompany.simpleapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";
    MainActivityFragment mainFrag;
    DoSomethingThread randomWork;
    boolean isBlack = true;
    boolean isRunning = false;
    BroadcastReceiver resultReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            mainFrag = new MainActivityFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, mainFrag).commit();
        }
        resultReceiver = createBroadcastReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(
                resultReceiver,
                new IntentFilter("com.mycompany.random.generation"));
        Log.v(TAG, "created Main Activity");
    }

    @Override
    protected void onDestroy() {
        if (resultReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(
                    resultReceiver);
        }
        super.onDestroy();
    }

    private BroadcastReceiver createBroadcastReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateResults(intent.getStringExtra("result"));
            }
        };
    }

    public void startGenerating() {
        if (!isRunning) {
            randomWork = new DoSomethingThread();
            randomWork.start();
            isRunning = true;
        }
    }

    public void stopGenerating() {
        if (isRunning) {
            randomWork.interrupt();
            updateResults(getString(R.string.service_off));
            isBlack = false;
            isRunning = false;
        }
    }

    public void updateResults(String results) {
        mainFrag.getResultsTextView().setText(results);
        if (isBlack) {
            mainFrag.getResultsTextView().setTextColor(getResources().getColor(R.color.red));
            isBlack = false;
        } else {
            mainFrag.getResultsTextView().setTextColor(getResources().getColor(R.color.black));
            isBlack = true;
        }
    }

    public class DoSomethingThread extends Thread {

        private static final String TAG = "DoSomethingThread";
        private static final int DELAY = 5000; // 5 seconds
        private static final int RANDOM_MULTIPLIER = 10;

        @Override
        public void run() {
            Log.v(TAG, "doing work in Random Number Thread");
            while (true) {
                int randNum = (int) (Math.random() * RANDOM_MULTIPLIER);
                publishProgress(randNum);
                try {
                    Thread.sleep(DELAY);
                } catch (InterruptedException e) {
                    Log.v(TAG, "Interrupting and stopping the Random Number Thread");
                    return;
                }
            }
        }

        private void publishProgress(int randNum) {
            Log.v(TAG, "reporting back from the Random Number Thread");
            final String text = String.format(getString(R.string.service_msg),
                    randNum);
            Intent intent = new Intent("com.mycompany.random.generation");
            intent.putExtra("result", text);
            LocalBroadcastManager.getInstance(MainActivity.this)
                    .sendBroadcast(intent);
        }
    }
}
