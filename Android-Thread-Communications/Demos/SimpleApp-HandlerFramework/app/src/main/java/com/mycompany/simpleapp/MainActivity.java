package com.mycompany.simpleapp;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.lang.ref.WeakReference;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";
    MainActivityFragment mainFrag;
    DoSomethingThread randomWork;
    boolean isBlack = true;
    boolean isRunning = false;
    Handler resultHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            mainFrag = new MainActivityFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, mainFrag).commit();
        }
        resultHandler = new HandlerExtension(this);
        Log.v(TAG, "created Main Activity");
    }

    public void startGenerating() {
        if (!isRunning) {
            randomWork = new DoSomethingThread();
            randomWork.start();
            isRunning = true;
        }
    }

    public void stopGenerating() {
        if (isRunning) {
            randomWork.interrupt();
            updateResults(getString(R.string.service_off));
            isBlack = false;
            isRunning = false;
        }
    }

    public void updateResults(String results) {
        mainFrag.getResultsTextView().setText(results);
        if (isBlack) {
            mainFrag.getResultsTextView().setTextColor(getResources().getColor(R.color.red));
            isBlack = false;
        } else {
            mainFrag.getResultsTextView().setTextColor(getResources().getColor(R.color.black));
            isBlack = true;
        }
    }

    private static class HandlerExtension extends Handler {

        private final WeakReference<MainActivity> currentActivity;

        public HandlerExtension(MainActivity activity){
            currentActivity = new WeakReference<MainActivity>(activity);
        }

        @Override
        public void handleMessage(Message message){
            MainActivity activity = currentActivity.get();
            if (activity!= null){
                activity.updateResults(message.getData().getString("result"));
            }
        }
    }

    public class DoSomethingThread extends Thread {

        private static final String TAG = "DoSomethingThread";
        private static final int DELAY = 5000; // 5 seconds
        private static final int RANDOM_MULTIPLIER = 10;

        @Override
        public void run() {
            Log.v(TAG, "doing work in Random Number Thread");
            while (true) {
                int randNum = (int) (Math.random() * RANDOM_MULTIPLIER);
                publishProgress(randNum);
                try {
                    Thread.sleep(DELAY);
                } catch (InterruptedException e) {
                    Log.v(TAG,
                            "Interrupting and stopping the Random Number Thread");
                    return;
                }
            }
        }

        private void publishProgress(int randNum) {
            Log.v(TAG, "reporting back from the Random Number Thread");
            String text = String.format(getString(R.string.service_msg),
                    randNum);
            Bundle msgBundle = new Bundle();
            msgBundle.putString("result", text);
            Message msg = new Message();
            msg.setData(msgBundle);
            resultHandler.sendMessage(msg);
        }
    }
}
