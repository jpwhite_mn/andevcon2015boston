package com.mycompany.simpleapp;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";
    MainActivityFragment mainFrag;
    DoSomethingTask randomWork;
    boolean isBlack = true;
    boolean isRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            mainFrag = new MainActivityFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, mainFrag).commit();
        }
        Log.v(TAG, "created Main Activity");
    }

    public void startGenerating() {
        if (!isRunning) {
            randomWork = new DoSomethingTask();
            randomWork.execute();
            isRunning = true;
        }
    }

    public void stopGenerating() {
        if (isRunning) {
            randomWork.cancel(true);
            updateResults(getString(R.string.service_off));
            isBlack = false;
            isRunning = false;
        }
    }

    public void updateResults(String results) {
        mainFrag.getResultsTextView().setText(results);
        if (isBlack) {
            mainFrag.getResultsTextView().setTextColor(getResources().getColor(R.color.red));
            isBlack = false;
        } else {
            mainFrag.getResultsTextView().setTextColor(getResources().getColor(R.color.black));
            isBlack = true;
        }
    }

    public class DoSomethingTask extends AsyncTask<Void, String, Void> {

        private static final String TAG = "DoSomethingTask";
        private static final int DELAY = 5000; // 5 seconds
        private static final int RANDOM_MULTIPLIER = 10;

        @Override
        protected void onPreExecute() {
            Log.v(TAG, "starting the Random Number Task");
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.v(TAG, "reporting back from the Random Number Task");
            updateResults(values[0].toString());
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled(Void result) {
            Log.v(TAG, "cancelled the Random Number Task");
            super.onCancelled(result);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.v(TAG, "doing work in Random Number Task");
            String text="";
            while (true) {
                if (isCancelled()) {
                    break;
                }
                int randNum = (int) (Math.random() * RANDOM_MULTIPLIER);
                text = String.format(getString(R.string.service_msg), randNum);
                publishProgress(text);
                try {
                    Thread.sleep(DELAY);
                } catch (InterruptedException e) {
                    Log.v(TAG, "Interrupting the Random Number Task");
                }
            }
            return null;
        }

    }
}
