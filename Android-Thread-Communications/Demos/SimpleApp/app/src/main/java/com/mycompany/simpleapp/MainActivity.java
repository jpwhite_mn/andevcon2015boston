package com.mycompany.simpleapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";
    MainActivityFragment mainFrag;
    DoSomething randomWork;
    boolean isBlack = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            mainFrag = new MainActivityFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, mainFrag).commit();
        }
        Log.v(TAG, "created Main Activity");
    }

    public void startGenerating() {
        randomWork = new DoSomething();
        randomWork.run();
    }

    public void stopGenerating() {
        updateResults(getString(R.string.service_off));
        isBlack = true;
    }

    public void updateResults(String results) {
        mainFrag.getResultsTextView().setText(results);
        if (isBlack) {
            mainFrag.getResultsTextView().setTextColor(getResources().getColor(R.color.red));
            isBlack = false;
        } else {
            mainFrag.getResultsTextView().setTextColor(getResources().getColor(R.color.black));
            isBlack = true;
        }
    }

    public class DoSomething {

        private static final String TAG = "DoSomething";
        private static final int DELAY = 5000; // 5 seconds
        private static final int RANDOM_MULTIPLIER = 10;

        public void run() {
            Log.v(TAG, "doing work in Random Number Generator");
            while (true) {
                int randNum = (int) (Math.random() * RANDOM_MULTIPLIER);
                publishProgress(randNum);
                try {
                    Thread.sleep(DELAY);
                } catch (InterruptedException e) {
                    Log.v(TAG,
                            "Interrupting and stopping the Random Number Generator");
                    return;
                }
            }
        }

        private void publishProgress(int randNum) {
            Log.v(TAG, "reporting back from the Random Number Thread");
            final String text = String.format(getString(R.string.service_msg),
                    randNum);
            updateResults(text);
        }
    }
}
