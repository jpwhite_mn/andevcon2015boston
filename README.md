**Welcome AnDevCon participants!**

This repository contains materials for Jim White's tutorial and class for AnDevCon Boston 2015.

* [Android UI Testing with Espresso](http://www.andevcon.com/boston/tutorials#AndroidUITestingwithEspresso)

* [Android Thread Communications](http://www.andevcon.com/boston/classes#AndroidThreatCommunications)

Copy the materials from this Bit Bucket account using Git.  See the Bit Bucket [documentation ](https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+101)for help with Git or Bit Bucket.

For those attending the Android UI Testing with Espresso tutorial, you need to install the latest Android Studio and then update Android Studio with Android Support Repository per the installation guide in this repository (see the [install doc](https://bitbucket.org/jpwhite_mn/andevcon2015boston/src/45834a23e14c432b014c474a6bdc641f66978bc8/Android%20Studio%20Setup.docx?at=master))