package com.mycompany.lab3;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private static final int CONTACT_PICKER_RESULT = 1001;

    private EditText eMailET, nameET, favoriteTeamET;
    private Spinner favoriteSportSP;
    private CheckBox seniorCB;
    private RadioButton maleRB, femaleRB;
    private Button submitBT;

    boolean serviceBound = false;
    PDIService pdiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        eMailET = (EditText) findViewById(R.id.emailET);
        nameET = (EditText) findViewById(R.id.nameET);
        favoriteTeamET = (EditText) findViewById(R.id.teamET);
        favoriteSportSP = (Spinner) findViewById(R.id.sportSP);
        seniorCB = (CheckBox) findViewById(R.id.seniorCB);
        maleRB = (RadioButton) findViewById(R.id.maleRB);
        femaleRB = (RadioButton) findViewById(R.id.femaleRB);
        submitBT = (Button) findViewById(R.id.submitBT);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, PDIService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (serviceBound) {
            unbindService(mConnection);
            serviceBound = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.about_title)
                    .setMessage(R.string.about_message)
                    .setNeutralButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void submit(View view) {
        if (eMailET.getText() != null && eMailET.getText().length() > 0) {
            Toast.makeText(this, R.string.data_saved, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, R.string.data_needed, Toast.LENGTH_LONG).show();
        }
    }

    public void findContact(View view) {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(contactPickerIntent, CONTACT_PICKER_RESULT);
    }

    public void findMe(View view) {
        if (serviceBound && eMailET.getText() != null && eMailET.getText().length() > 0) {
            Person p = pdiService.fetchPersonalData(eMailET.getText().toString());
            nameET.setText(p.getName());
            nameET.setEnabled(true);
            maleRB.setChecked(p.isMale());
            maleRB.setEnabled(true);
            femaleRB.setChecked(p.isFemale());
            femaleRB.setEnabled(true);
            seniorCB.setChecked(p.isSenior());
            seniorCB.setEnabled(true);
            favoriteSportSP.setSelection(p.getFavoriteSportId());
            favoriteSportSP.setClickable(true);
            favoriteTeamET.setText(p.getFavoriteTeam());
            favoriteTeamET.setEnabled(true);
            submitBT.setEnabled(true);
        } else {
            nameET.setText("");
            nameET.setEnabled(false);
            maleRB.setChecked(false);
            maleRB.setEnabled(false);
            femaleRB.setEnabled(false);
            seniorCB.setEnabled(false);
            favoriteSportSP.setSelection(0);
            favoriteSportSP.setClickable(false);
            favoriteTeamET.setText("");
            favoriteTeamET.setEnabled(false);
            submitBT.setEnabled(false);
            Toast.makeText(this, R.string.data_needed, Toast.LENGTH_LONG).show();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CONTACT_PICKER_RESULT:
                    Uri result = data.getData();
                    String id = result.getLastPathSegment();
                    Cursor cursor = getContentResolver().query(
                            ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + "=?",
                            new String[]{id}, null);
                    if (cursor.moveToFirst()) {
                        int emailIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA);
                        String email = cursor.getString(emailIdx);
                        eMailET.setText(email);
                    }
                    cursor.close();
                    break;
            }
        }
    }


    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            PDIService.LocalBinder binder = (PDIService.LocalBinder) service;
            pdiService = binder.getService();
            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            serviceBound = false;
        }
    };

}
