package com.mycompany.lab3;

/**
 * Created by Jim on 6/26/2015.
 */
public class Person {

    private String name;
    private boolean male;
    private boolean female;
    private boolean senior;
    private int favoriteSportId;
    private String favoriteTeam;

    public Person(String name, boolean male, boolean female, boolean senior, int favoriteSportId, String favoriteTeam) {
        this.name = name;
        this.male = male;
        this.female = female;
        this.senior = senior;
        this.favoriteSportId = favoriteSportId;
        this.favoriteTeam = favoriteTeam;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public boolean isFemale() {
        return female;
    }

    public void setFemale(boolean female) {
        this.female = female;
    }

    public boolean isSenior() {
        return senior;
    }

    public void setSenior(boolean senior) {
        this.senior = senior;
    }

    public int getFavoriteSportId() {
        return favoriteSportId;
    }

    public void setFavoriteSportId(int favoriteSportId) {
        this.favoriteSportId = favoriteSportId;
    }

    public String getFavoriteTeam() {
        return favoriteTeam;
    }

    public void setFavoriteTeam(String favoriteTeam) {
        this.favoriteTeam = favoriteTeam;
    }

}
