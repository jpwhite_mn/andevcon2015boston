package com.mycompany.lab3;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class PDIService extends Service {
    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        PDIService getService() {
            // Return this instance of PDIService so clients can call public methods
            return PDIService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public Person fetchPersonalData(String emailAddress) {
        // simulate the fetch and return of personal data information in the Intent
        if (emailAddress != null && emailAddress.length() > 0) {
            return  new Person("Barrack Obama", true, false, false, 1, "Chicago Bulls");
        }
        return null;
    }

}
